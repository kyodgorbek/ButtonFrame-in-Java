# ButtonFrame-in-Java



import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class ButtonFrame extends JFrame {
  
  public static void main(String[] args){
   
   private JPanel buttonPanel;
   private static final int DEFAULT_WIDTH = 300;
   private static final int DEFAULT_HEIGHT = 200;
   
   public ButtonFrame()
   {
     setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
     
     // create buttons
     JButton yellowButton = new JButton("Yellow");
     JButton blueButton = new Jbutton("Blue");
     JButton redButton = new JButton("Red");
     
     buttonPanel = new JPanel();
     
     // add buttons to panel
     buttonPanel.add(yellowButton);
     buttonPanel.add(blueButton);
     buttonPanel.add(redButton);
     
     // add panel to frame
     add(buttonPanel);
     
     // create button actions
     ColorAction yellowAction = new ColorAction(Color.YELLOW);
     ColorAction blueAction = new ColorAction(Color.BLUE);
     ColorAction redAction = new ColorAction(Color.RED);
     
     // associate action with buttons
     yellowButton.addActionListener(yellowAction);
     blueButton.addActionListener(blueAction);
     redButton.addActionListener(redAction);
     
     private class ColorAction implements ActionListener
     {
      private Color backgroundColor;
      
      public ColorAction(Color c)
      {
         backgroundColor = c; 
      }
      
      public void actionPerformed(ActionEvent event)
      {
        buttonPanel.setBackground(backgroundColor);
      }
     }
    }    
   
   
   
   
   
   
   
   
   
   
   
   
   
